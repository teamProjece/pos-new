<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class MY_Controller extends CI_Controller
{
    protected $data = array();
    function __construct()
    {
        parent::__construct();
    }
    // layout
    public function layout(){
        $this->template['libary_header'] = $this->load->view('template/libary-header', $this->data,TRUE);
        $this->template['header'] = $this->load->view('template/header', $this->data,TRUE);
        $this->template['slibar'] = $this->load->view('template/slibar', $this->data,TRUE);
        $this->template['footer'] = $this->load->view('template/footer', $this->data,TRUE);
        $this->template['libary_footer'] = $this->load->view('template/libary-footer', $this->data,TRUE);
        $this->template['page'] = $this->load->view($this->page, $this->data,TRUE);
        $this->load->view("template/main", $this->template);
    }
}
    