<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 2 | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <?php if($libary_header) echo $libary_header; ?>
 </head>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">
    <?php if($header) echo $header; ?>
    <?php if($slibar) echo $slibar; ?>
    <?php if($page) echo $page; ?>
    <?php if($footer) echo $footer; ?>
  </div>
    <?php if($libary_footer) echo $libary_footer; ?>
</body>
</html>